<?php
$post_type = get_post_type();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$author_id = get_the_author_meta('ID');
$cargo = get_field('cargo', 'user_'. $author_id );
$id = get_the_ID();
$i ++;
$a ++;

if($post_type == 'podcast'){

echo '<li class="single">
        <a data-fancybox data-src="#hidden-content'.$id.'" href="javascript:;">
        	<div class="block-img">
                <div class="imagem">
                    <div class="img" style="background-image:url('.$image[0].');">
                    </div>
                </div>
            </div>
            <div class="conteudo">
                <div class="topo">';
                    if(get_field('tempo_de_audio')){
                        echo '<div class="play">
                                        <img src="'.get_template_directory_uri().'/images/play.svg"> 
                                        <span>'.get_field('tempo_de_audio').'</span>
                                    </div>';
                    }

                $categories = wp_get_post_categories( get_the_ID() );
                //loop through them
                foreach($categories as $c){
                  $cat = get_category( $c );
                  //get the name of the category
                  $cat_id = get_cat_ID( $cat->name );
                  //make a list item containing a link to the category
                  echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                <img src="'.get_field('icone', $cat).'"> 
                                <span>'.$cat->name.'</span>
                              </div>';
                }
                    
                echo '</div>

                <div class="txt">';
                    if(get_field('num_ep')){
                        echo '<span class="num_ep">
                                    #EP'.get_field('num_ep').'
                                    </span>';
                    }

                    echo '<h3>'.get_the_title().'</h3>
                    <p>'.get_the_excerpt().'</p>
                    <div class="author">
                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                        <div class="info">
                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                            <span>'.$cargo.'</span>
                        </div>
                    
                    </div>
                </div>
            </div>
        </a>

        <div class="iframe" style="display: none;" id="hidden-content'.$id.'">
            '.get_field('iframe_soundcloud').'
        </div>
    </li>';

}



if($post_type == 'video'){


echo '<li class="single">
        <a data-fancybox href="'.get_field('video', false, false).'">
            <div class="side">
            <div class="video-responsive">
                <div class="conteudo">
                    <div class="topo">
                        <img src="'.get_template_directory_uri().'/images/play-video.svg">'; 
                    echo '</div>';

                    if(get_field('tempo_de_video')){
                        echo '<span class="time">'.get_field('tempo_de_video').'</span>';
                    }

                echo '</div>
                <div class="img" style="background-image:url('.$image[0].');">
                </div>
            </div>
            </div>
            <div class="info">
                <div class="txt">';
                    $categories = wp_get_post_categories( get_the_ID() );
                    foreach($categories as $c){
                      $cat = get_category( $c );
                      //get the name of the category
                      $cat_id = get_cat_ID( $cat->name );
                      //make a list item containing a link to the category
                      echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                    <img src="'.get_field('icone', $cat).'"> 
                                    <span>'.$cat->name.'</span>
                                  </div>';
                    }

                    echo '<h3>'.get_the_title().'</h3>
                </div>
            </div>
        </a>
    </li>';

}




if($post_type == 'post'){

echo '<li class="single">
        <a href="'.get_the_permalink().'" title="'.get_the_title().'" >
            <div class="conteudo">
                <div class="topo">';

                $categories = wp_get_post_categories( get_the_ID() );
                //loop through them
                foreach($categories as $c){
                  $cat = get_category( $c );
                  //get the name of the category
                  $cat_id = get_cat_ID( $cat->name );
                  //make a list item containing a link to the category
                  echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                <img src="'.get_field('icone', $cat).'"> 
                                <span>'.$cat->name.'</span>
                              </div>';
                }
                    
                echo '</div>

                <div class="txt">
                    <h3>'.get_the_title().'</h3>
                    <div class="author">
                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                        <div class="info">
                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                            <span>'.$cargo.'</span>
                        </div>
                    
                    </div>
                </div>
            </div>
            <div class="img" style="background-image:url('.$image[0].');">
            </div>
        </a>
    </li>';

}





if($post_type == 'edicao'){
echo '<li class="single">
    	<a href="'.get_permalink().'">
            <div class="side">
                <div class="revista-side">
                    <div class="conteudo">
                        <div class="img" style="background-image:url('.$image[0].');">
                        </div>
                	</div>
                </div>
            </div>
            <div class="info">
                <div class="txt">';
                    echo '<h3>'.get_the_title().'</h3>
                    <p>'.get_the_excerpt().'</p>
                    <span class="btn">
                    Ler Edição
                    </span>
                </div>
            </div>
        </a>
    </li>';

}





if($post_type == 'guia'){
 echo '<li>
                <a href="'.get_permalink().'" title="'.get_the_title().'">
                <div class="img" style="background-image:url('.$image[0].');">
                </div>
                <h4>'.get_the_title().'</h4>';
                if(get_field('cargo')){
                    echo '<span class="cargo">'.get_field('cargo').'</span>';
                }
				echo '<p>'.get_the_excerpt().'</p>';
   				echo '<span class="btn_light">VER PROFISISIONAL</span>';
echo '</a></li>';
}
?>