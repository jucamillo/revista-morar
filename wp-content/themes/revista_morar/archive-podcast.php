<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */

get_header();
?>
<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_podcast', 'option') ): ?>
            	<h3><?php the_field('subtitulo_podcast', 'option'); ?></h3>
            <?php endif; ?>
            <?php if( get_field('titulo_podcast', 'option') ): ?>
            	<h1><?php the_field('titulo_podcast', 'option'); ?></h1>
            <?php endif; ?>
		</div>
	</div>
</section>
<section class="topo destaque_archive archive_podcast">
	<div class="container">
		<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
			
            <?php if( get_field('logo_morar', 'option') ): ?>
            	<div class="logo_morar">
            		<img src="<?php the_field('logo_morar', 'option'); ?>">
            	</div>
            <?php endif; ?>

            <?php the_field('texto_morar', 'option'); ?>
		</div>
		<div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
			<h2>DESTAQUE</h2>
			<?php 

		    $bannerArgs = array( 
		        'post_type' => 'podcast', 
		        'posts_per_page' => 1, 
		        'orderby'=>'id',
		        'order'=>'desc'
		    );
		    $bannerLoop = new WP_Query( $bannerArgs ); 
		    echo '<ul class="podcast">';
		    $i = 0;
		    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();


			$post_type = get_post_type();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			$author_id = get_the_author_meta('ID');
			$cargo = get_field('cargo', 'user_'. $author_id );
			$i ++;

			echo '<li class="single">
			        <a data-fancybox data-src="#hidden-content-destaque'.$i.'" href="javascript:;">
			        	<div class="block-img">
			                <div class="imagem">
			                    <div class="img" style="background-image:url('.$image[0].');">
			                    </div>
			                </div>
			            </div>
			            <div class="conteudo">
			                <div class="topo">';
			                    if(get_field('tempo_de_audio')){
			                        echo '<div class="play">
			                                        <img src="'.get_template_directory_uri().'/images/play.svg"> 
			                                        <span>'.get_field('tempo_de_audio').'</span>
			                                    </div>';
			                    }

			                $categories = wp_get_post_categories( get_the_ID() );
			                //loop through them
			                foreach($categories as $c){
			                  $cat = get_category( $c );
			                  //get the name of the category
			                  $cat_id = get_cat_ID( $cat->name );
			                  //make a list item containing a link to the category
			                  echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
			                                <img src="'.get_field('icone', $cat).'"> 
			                                <span>'.$cat->name.'</span>
			                              </div>';
			                }
			                    
			                echo '</div>

			                <div class="txt">';
			                    if(get_field('num_ep')){
			                        echo '<span class="num_ep">
			                                    #EP'.get_field('num_ep').'
			                                    </span>';
			                    }

			                    echo '<h3>'.get_the_title().'</h3>
			                    <p>'.get_the_excerpt().'</p>
			                    <div class="author">
			                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
			                        <div class="info">
			                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
			                            <span>'.$cargo.'</span>
			                        </div>
			                    
			                    </div>
			                </div>
			            </div>
			        </a>

			        <div class="iframe" style="display: none;" id="hidden-content-destaque'.$i.'">
			            '.get_field('iframe_soundcloud').'
			        </div>
			    </li>';
    		endwhile;

		    echo '</ul>'; ?>
		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_podcast">
	<div class="container">
		<div class="col-xs-12 up1">
    		<?php if(get_field('ad_Podcast', 'option')): ?>
			<div class="separator-wrap top">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_Podcast', 'option').'"]'); ?>
			</div>
			<?php endif; ?>
			<h2>EPSÓDIOS ANTERIORES</h2>
			<?php echo do_shortcode('[ajax_load_more container_type="ul" css_classes="podcast" post_type="podcast" posts_per_page="4" offset="1" scroll="false" button_label="CARREGAR MAIS EPISÓDIOS" button_loading_label="CARREGANDO EPISÓDIOS..." button_done_label="TODOS OS EPISÓDIOS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum episódio cadastrado."]');?>


		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
