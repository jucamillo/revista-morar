<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package revista_morar
 */

get_header();
?>


		<?php
		while ( have_posts() ) :
			the_post(); ?>

<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_ed', 'option') ): ?>
            	<h3><?php the_field('subtitulo_ed', 'option'); ?></h3>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<section class="topo destaque_archive archive_edicao single_edicao">
	<div class="container">
		<div class="col-xs-12">

            <?php if( get_field('codigo_iframe') ): ?>
            	<?php the_field('codigo_iframe'); ?>
            <?php endif; ?>

		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_edicao">
	<div class="container">
		<div class="col-xs-12 up1">
    		<?php if(get_field('ad_ed_interna', 'option')): ?>
			<div class="separator-wrap top">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_ed_interna', 'option').'"]'); ?>
			</div>
			<?php endif; ?>
			<h2>OUTRAS EDIÇÕES</h2>
			<?php  echo do_shortcode('[ajax_load_more container_type="ul" css_classes="edicao list" post_type="edicao" posts_per_page="2" scroll="false" button_label="CARREGAR MAIS EDIÇÕES" button_loading_label="CARREGANDO EDIÇÕES..." button_done_label="TODOS AS EDIÇÕES FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhuma edição cadastrada." post__not_in="'.get_the_id().'"]');?>

		</div>
	</div>
</section>

<?php
endwhile;
get_footer();
