<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if ( !is_singular() ) :


                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                echo '<li class="item">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <span class="data">
                                    <b>'.get_the_date('d').'</b>
                                    <em>'.get_the_date('M').'</em>
                                </span>
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  //make a list item containing a link to the category
                                  echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
                                }
                    echo '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
                                <p>'.strip_tags( get_the_excerpt() ).'</p>
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">Leia Mais <i class="fas fa-angle-right"></i></a></div>
                            </li>';

         endif; ?>