<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */


if ( !is_front_page() ) : ?>
<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo') ): ?>
            	<h3><?php the_field('subtitulo'); ?></h3>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<?php	endif; ?>


<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		the_content();
		?>

</section><!-- #post-<?php the_ID(); ?> -->
