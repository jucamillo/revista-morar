<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package revista_morar
 */

?>
	</section>
    <?php if(get_field('chamada_guia', 'option')): ?>
    <section class="guia_bg" style="background-image: url(<?php echo get_field('bg_guia', 'option') ?>);">
        <div class="container">
            <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
                <?php echo get_field('chamada_guia', 'option') ?>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <?php if(get_field('chamada_revista', 'option')): ?>
    <section class="revista" style="background-image: url(<?php echo get_field('bg_revista', 'option') ?>);">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <img src="<?php echo get_field('capa', 'option') ?>">
                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                <?php echo get_field('chamada_revista', 'option') ?>
                
            </div>
        </div>
    </section>
    <?php endif; ?>

	<footer id="colophon" class="site-footer">
		<div class="container">
            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">

                <?php if( get_field('logo', 'option') ): ?>
                    <a href="<?php echo get_home_url(); ?>" class="branding">
                        <img src="<?php the_field('logo', 'option'); ?>">
                    </a>
                <?php endif; ?>
                <?php if( get_field('sobre', 'option') ): ?>
                    <p class="sobre"><?php the_field('sobre', 'option'); ?>"></p>
                <?php endif; ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                ) );
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

                <?php if( get_field('contato', 'option') ): ?>
                    <?php the_field('contato', 'option'); ?>
                <?php endif; ?>


            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 news">
                <?php if( get_field('novidades', 'option') ): ?>
                    <?php the_field('novidades', 'option'); ?>
                <?php endif; ?>
            </div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

<script type="text/javascript">
	

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });
});

        jQuery('body.home ul.podcast').owlCarousel({
            margin:0,
            responsiveClass:true,
            dots: true,
                    loop:true,
            nav:false,
            autoHeight:false,
            autoplay: false,
            autoplayTimeout: 10000,
            responsive:{
                0:{
                    items:1,
                    stagePadding: 10,
                    margin:10
                },
                374:{
                    items:1,
                    stagePadding: 30
                },
                500:{
                    items:1,
                    stagePadding: 50
                },
                768:{
                    items:2
                },
                1200:{
                    items:2
                },
                1280:{
                    items:3
                }
            }
        })




jQuery(document).delegate('header nav.main-navigation .menu-toggle', 'click', function(event) {
    event.preventDefault();
    jQuery('header  .main-navigation').toggleClass('toggled');
});



jQuery(document).delegate('header:not(.search_active) form.search-form button', 'click', function(event) {
    event.preventDefault();
    jQuery('header').addClass('search_active');
    jQuery('header  .main-navigation').removeClass('toggled');
});

jQuery(document).delegate('.filtro > ul > li > span', 'click', function(event) {
    event.preventDefault();
    
    jQuery(this).parent('li').toggleClass('active');
});


jQuery(document).delegate('footer .menu-principal-container:not(.active)', 'click', function(event) {
    event.preventDefault();
    
    jQuery(this).addClass('active');
});




jQuery(document).mouseup(function(e) 
{
    var container = jQuery("form.search-form");

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
    jQuery('header').removeClass('search_active');
    }


    var filtroli = jQuery(".filtro > ul > li");

    if (!filtroli.is(e.target) && filtroli.has(e.target).length === 0) 
    {
        jQuery('.filtro > ul > li').removeClass('active');
    }


    /*
    var menu = jQuery("header nav.main-navigation .menu-principal-container");

    if (!menu.is(e.target) && menu.has(e.target).length === 0) 
    {
        jQuery('header nav.main-navigation').removeClass('toggled');
    }*/


    var map = jQuery("footer .menu-principal-container.active ul");

    if (!map.is(e.target) && map.has(e.target).length === 0) 
    {
        jQuery('footer .menu-principal-container').removeClass('active');
    }




});



var maskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskBehavior.apply({}, arguments), options);
    }
};

jQuery('input[name="telefone_1"]').mask(maskBehavior, options);
jQuery('input[name="telefone_2"]').mask(maskBehavior, options);
jQuery('input[name="telefone"]').mask(maskBehavior, options);



if (jQuery(window).width() < 992) {
    jQuery(function(){
        jQuery('header .menu-item-has-children').append('<button class="open-sub"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"/></svg></button>');


        jQuery(document).delegate('header .menu-item-has-children button.open-sub', 'click', function(event) {

            event.preventDefault();

            jQuery(this).parent().toggleClass('menu-open-sub');
        });
    });
    
}  





var left = {
    delay: 100,
    distance: '100px',
    duration: 1300,
    origin: 'left',
    easing: 'ease-in-out',
};
var left2 = {
    delay: 400,
    distance: '100px',
    duration: 1300,
    origin: 'left2',
    easing: 'ease-in-out',
};


var right = {
    delay: 100,
    distance: '100px',
    duration: 1300,
    origin: 'right',
    easing: 'ease-in-out',
};

var right2 = {
    delay: 400,
    distance: '100px',
    duration: 1300,
    origin: 'right2',
    easing: 'ease-in-out',
};





var up1 = {
    delay: 100,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up2 = {
    delay: 400,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up3 = {
    delay: 700,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up4 = {
    delay: 1000,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};




var down = {
    delay: 100,
    distance: '100px',
    duration: 1000,
    origin: 'top',
    easing: 'ease-in-out',
};




var fade = {
    delay: 100,
    distance: '100px',
    duration: 1000,
    origin: 'bottom',
    easing: 'ease-in-out',
};





jQuery(document).ready(function(){



    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up2', up2);
    ScrollReveal().reveal('.up3', up3);
    ScrollReveal().reveal('.up4', up4);
    ScrollReveal().reveal('.left', left);
    ScrollReveal().reveal('.right', right);

    ScrollReveal().reveal('.left2', left2);
    ScrollReveal().reveal('.right2', right2);

    ScrollReveal().reveal('.down', down);
    ScrollReveal().reveal('.fade', fade);





});



            setTimeout( function()  {
                jQuery('.banner_destaque').addClass('active');
                jQuery('.title_section').addClass('active');



            },500);

            setTimeout( function()  {
                jQuery('.page-revista-morar #quem-somos').addClass('active');
                jQuery('.page-colunas section.vc_section').addClass('active');
                jQuery('.archive_author ul.equipe.pessoas.single').addClass('active');
                //jQuery('.single_noticia article').addClass('active');


                jQuery('section.destaque_archive.archive_podcast').addClass('active');
                jQuery('section.destaque_archive.archive_video').addClass('active');
                jQuery('section.destaque_archive.archive_edicao').addClass('active');
                jQuery('section.list-archive-section.archive_guia').addClass('active');
                jQuery('.page-contato .vc_section').addClass('active');


            },700);


/*
jQuery( "ul.podcast li" ).each(function( index ) {
    var iframeElement   = jQuery(this).find('iframe');
    var iframeElement = iframeElement.id;
    var widget1         = SC.Widget(iframeElement);
    var widget2         = SC.Widget(iframeElementID);

    jQuery('ul.podcast li a').click(function() {
        widget2.play();
        widget1.play();
        iframeElement.play();
        iframeElement.play();

        console.log('aaaaaaa');
    });

});

*/

    jQuery('.video-responsive').fancybox({
        toolbar  : false,
        smallBtn : true,
        iframe : {
            preload : false
        }
    })


</script>
</body>
</html>
