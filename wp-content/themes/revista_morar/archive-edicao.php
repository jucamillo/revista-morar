<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */

get_header();
?>
<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_ed', 'option') ): ?>
            	<h3><?php the_field('subtitulo_ed', 'option'); ?></h3>
            <?php endif; ?>
            <?php if( get_field('titulo_ed', 'option') ): ?>
            	<h1><?php the_field('titulo_ed', 'option'); ?></h1>
            <?php endif; ?>
		</div>
	</div>
</section>
<section class="topo destaque_archive archive_edicao ">
	<div class="container">
		<div class="col-xs-12 ">
			<?php
			    $bannerArgs = array( 
			        'post_type' => 'edicao', 
			        'posts_per_page' => 1, 
			        'orderby'=>'id',
			        'order'=>'DESC'
			    );
			    $bannerLoop = new WP_Query( $bannerArgs ); 
			   


			    echo '<ul class="edicao">';
			    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
			        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        			echo '<li class="single">
                        	<a href="'.get_permalink().'">
	                            <div class="side">
		                            <div class="revista-side">
		                                <div class="conteudo">
			                                <div class="img" style="background-image:url('.$image[0].');">
			                                </div>
		                            	</div>
		                            </div>
	                            </div>
	                            <div class="info">
	                                <div class="txt">';
	                                    echo '<h3>'.get_the_title().'</h3>
	                                    <p>'.get_the_excerpt().'</p>
	                                    <span class="btn">
	                                    Ler Edição
	                                    </span>
	                                </div>
	                            </div>
	                        </a>
	                    </li>';
			    endwhile;
			    echo '</ul>'; 

			    ?>

		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_edicao">
	<div class="container">
		<div class="col-xs-12 up1">
    		<?php if(get_field('ad_ed', 'option')): ?>
			<div class="separator-wrap top">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_ed', 'option').'"]'); ?>
			</div>
			<?php endif; ?>
			<h2>EDIÇÕES ANTERIORES</h2>
			<?php  echo do_shortcode('[ajax_load_more container_type="ul" css_classes="edicao list" post_type="edicao" posts_per_page="2" offset="1" scroll="false" button_label="CARREGAR MAIS EDIÇÕES" button_loading_label="CARREGANDO EDIÇÕES..." button_done_label="TODOS AS EDIÇÕES FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhuma edição cadastrada."]');?>


		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
