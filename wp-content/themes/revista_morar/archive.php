<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */
?>

<?php
get_header();
?>

<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_noticias', 'option') ): ?>
            	<h3><?php the_field('subtitulo_noticias', 'option'); ?></h3>
            <?php endif; ?>

			<h1><?php the_archive_title(); ?></h1>
		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_noticias">
	<div class="container">
		<div class="col-xs-12">
			<?php 

			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
			    $link = "https"; 
			else
			    $link = "http"; 
			$link .= "://"; 
			$link .= $_SERVER['HTTP_HOST']; 
			$link .= $_SERVER['REQUEST_URI']; 

			$pieces = explode("?order=", $link);
			$order = $pieces[1]; 
			$url = $pieces[0]; 
			?>
			<script type="text/javascript">
				
				jQuery(function(){
					jQuery(".filtro li.categories > ul").prepend('<li><a href="<?php echo get_home_url(); ?>/noticias">Todas</a></li>');
				});
			</script>
			<div class="filtro">
				<h4>
					Filtros:
				</h4>
				<ul>
				    <?php wp_list_categories( array(
				        'orderby'    => 'name',
				        'show_count' => false,
        				'title_li'   => '<span>'.get_the_archive_title().'</span>',
				    ) ); ?> 

				    <li class="order">
				    	<span>Ordenar por</span>
				    	<ul class="order">
				    		<li class="padrao">
				    			<a href="<?php echo $url; ?>" >
				    				Mais Recentes
				    			</a>
				    		</li>
				    		<li class="date">
				    			<a href="?order=date">
				    				Mais Antigos
				    			</a>
				    		</li>
				    		<li class="views">
				    			<a href="?order=views">
				    				Mais Populares
				    			</a>
				    		</li>
				    		<li class="title">
				    			<a href="?order=title" >
				    				Nome (A - Z)
				    			</a>
				    		</li>
				    	</ul>
				    </li>
				</ul>
			</div>
			<?php 
				if ($order == 'title'){
						echo '
							<script>
							jQuery(function(){
								jQuery(".filtro li").removeClass("active");

								jQuery(".filtro li.title").addClass("active");
							    var nome = jQuery(".filtro li.active a").html();
			        			jQuery(".filtro .order span").html(nome);

								});
							</script>

						';
						echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" orderby="title" archive="true" no_results_text="Ainda não temos nenhuma matéria cadastrada."]');

					} elseif ($order == 'date') {
						echo '
							<script>
							jQuery(function(){
								jQuery(".filtro .order li").removeClass("active");
								jQuery(".filtro .order li.date").addClass("active");
							    var nome = jQuery(".filtro .order li.active a").html();
			        			jQuery(".filtro .order span").html(nome);
								});
							</script>

						';
						echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" archive="true" no_results_text="Ainda não temos nenhuma matéria cadastrada."]');

					} elseif ($order == 'views') {
						echo '
							<script>
							jQuery(function(){
								jQuery(".filtro .order li").removeClass("active");
								jQuery(".filtro .order li.views").addClass("active");
							    var nome = jQuery(".filtro .order li.active a").html();
			        			jQuery(".filtro .order span").html(nome);
								});
							</script>

						';
						echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" orderby="meta_value_num" meta_key="wpb_post_views_count" archive="true" no_results_text="Ainda não temos nenhuma matéria cadastrada."]');

					}  else {
						echo '
							<script>
							jQuery(function(){
								jQuery(".filtro .order li").removeClass("active");
								jQuery(".filtro .order li.padrao").addClass("active");
							    var nome = jQuery(".filtro .order li.active a").html();
			        			jQuery(".filtro .order span").html(nome);
								});
							</script>

						';
						echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" archive="true" no_results_text="Ainda não temos nenhuma matéria cadastrada."]');
				}
			?>


    		<?php if(get_field('ad_noticia', 'option')): ?>
			<div class="separator-wrap">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_noticia', 'option').'"]'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php
//get_sidebar();
get_footer();
