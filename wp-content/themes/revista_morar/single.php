<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package revista_morar
 */

get_header();
?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gsap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

	


		<?php
		while ( have_posts() ) :
			the_post(); 
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );

        //wpb_set_post_views($post->ID);
        ?>


<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_noticias', 'option') ): ?>
            	<h3><?php the_field('subtitulo_noticias', 'option'); ?></h3>
            <?php endif; ?>

            <?php if( get_field('titulo_noticias', 'option') ): ?>
            	<h2><?php the_field('titulo_noticias', 'option'); ?></h2>
            <?php endif; ?>
		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_noticias single_noticia">
	<div class="container">
		<div class="col-xs-12">
			<article>
				<div class="top">
					
					<div class="tags">
						
					<?php


	                $categories = wp_get_post_categories( $post->ID );


	                foreach($categories as $c){
	                  $cat = get_category( $c );
	                  //get the name of the category
	                  $cat_id = get_cat_ID( $cat->name );
	                  //make a list item containing a link to the category
	                  echo '<a href="'.get_home_url().'/categoria/'.$cat->slug.'" class="tag" style="background:'.get_field('cor', $cat).';" title="'.$cat->name.'">
	                                <img src="'.get_field('icone', $cat).'"> 
	                                <span>'.$cat->name.'</span>
	                              </a>';
	                }
	                ?>
					</div>
					
					<h1><?php the_title(); ?></h1>
					<span class="data">
						<?php echo get_the_date(); ?>
					</span>

					<div class="meta">
						
		                <div class="author">
		                    <div class="foto" style="background-image:url('<?php echo get_avatar_url($author_id); ?>')">
		                    </div>
		                    <div class="info">
		                        <strong><?php echo get_the_author_meta('first_name', $author_id); ?> <?php echo get_the_author_meta('last_name', $author_id); ?></strong>
		                        <span><?php echo $cargo; ?></span>
		                    </div>
		                </div>


		                <ul class="rt_counts">
		                	<li>
		                		<svg width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M13 4.5C11.6641 6.5625 9.50781 8 7 8C4.49219 8 2.33594 6.5625 1 4.5C1.74219 3.35156 2.75781 2.36719 3.97656 1.74219C3.66406 2.27344 3.5 2.88281 3.5 3.5C3.5 5.42969 5.07031 7 7 7C8.92969 7 10.5 5.42969 10.5 3.5C10.5 2.88281 10.3359 2.27344 10.0234 1.74219C11.2422 2.36719 12.2578 3.35156 13 4.5ZM7.375 1.5C7.375 1.70312 7.20313 1.875 7 1.875C6.10938 1.875 5.375 2.60937 5.375 3.5C5.375 3.70312 5.20313 3.875 5 3.875C4.79688 3.875 4.625 3.70312 4.625 3.5C4.625 2.19531 5.69531 1.125 7 1.125C7.20313 1.125 7.375 1.29687 7.375 1.5ZM14 4.5C14 4.30469 13.9375 4.125 13.8438 3.96094C12.4063 1.60156 9.77344 -7.15256e-07 7 -7.15256e-07C4.22656 -7.15256e-07 1.59375 1.60156 0.15625 3.96094C0.0625 4.125 0 4.30469 0 4.5C0 4.69531 0.0625 4.875 0.15625 5.03906C1.59375 7.39844 4.22656 9 7 9C9.77344 9 12.4063 7.40625 13.8438 5.03906C13.9375 4.875 14 4.69531 14 4.5Z" fill="#645F7D"/>
								</svg>
								<span>
									<?php echo wpb_get_post_views($post->ID);?>
								</span>
		                	</li>
		                	<li>
		                		<svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M5.5 0.999999C7.9375 0.999999 10 2.375 10 4C10 5.625 7.9375 7 5.5 7C5.09375 7 4.69531 6.96094 4.30469 6.89062L3.89063 6.8125L3.54688 7.05469C3.39063 7.16406 3.22656 7.26562 3.0625 7.35937L3.33594 6.70312L2.57813 6.26562C1.57813 5.6875 1 4.85937 1 4C1 2.375 3.0625 0.999999 5.5 0.999999ZM5.5 -2.38419e-07C2.46094 -2.38419e-07 0 1.78906 0 4C0 5.26562 0.8125 6.39844 2.07812 7.13281C1.8125 7.78125 1.46875 8.08594 1.16406 8.42969C1.07813 8.53125 0.976563 8.625 1.00781 8.77344C1.03125 8.90625 1.14063 9 1.25781 9C1.26563 9 1.27344 9 1.28125 9C1.51563 8.96875 1.74219 8.92969 1.95313 8.875C2.74219 8.67187 3.47656 8.33594 4.125 7.875C4.5625 7.95312 5.02344 8 5.5 8C8.53906 8 11 6.21094 11 4C11 1.78906 8.53906 -2.38419e-07 5.5 -2.38419e-07ZM11.9219 9.13281C13.1875 8.39844 14 7.27344 14 6C14 4.69531 13.1406 3.53906 11.8203 2.8125C11.9375 3.19531 12 3.59375 12 4C12 5.39844 11.2813 6.6875 9.97656 7.64062C8.76563 8.51562 7.17969 9 5.5 9C5.27344 9 5.03906 8.98437 4.8125 8.96875C5.78906 9.60937 7.08594 10 8.5 10C8.97656 10 9.4375 9.95312 9.875 9.875C10.5234 10.3359 11.2578 10.6719 12.0469 10.875C12.2578 10.9297 12.4844 10.9688 12.7188 11C12.8438 11.0156 12.9609 10.9141 12.9922 10.7734C13.0234 10.625 12.9219 10.5313 12.8359 10.4297C12.5313 10.0859 12.1875 9.78125 11.9219 9.13281Z" fill="#645F7D"/>
								</svg>
								<span>
									<?php echo get_comments_number(); ?>
								</span>
		                	</li>
		                	
		                </ul>
					</div>
				</div>



				<?php revista_morar_post_thumbnail(); ?>
				<div class="miolo">
					
					<div id="trigger1"></div>
					<div class="social" id="pin1">
						<?php echo do_shortcode('[addtoany]'); ?>
					</div>

					<div class="content">
						<?php the_content( ); ?>
					</div>
				</div>
				
	    		<?php if(get_field('ad_noticia_interna', 'option')): ?>
				<div class="separator-wrap">	
					<?php echo do_shortcode('[the_ad id="'.get_field('ad_noticia_interna', 'option').'"]'); ?>
				</div>
				<?php endif; ?>

				<div class="comments">
					
				</div>
			</article>	

			<aside class="relacionados">
				<h2>RELACIONADOS</h2>

 				<?php 
 					$i = 0;
	                foreach($categories as $c){
	                	$i ++;
	                	if($i < 2){
		                   $cat = get_category( $c );
 							echo do_shortcode( '[my_related_posts category="'.$cat->slug.'"]' ); 
	                		
	                	};
	                };
				?>


			</aside>
		</div>
	</div>
</section>


<script type="text/javascript">
if( jQuery(window).width() > 767 ){
	var controller = new ScrollMagic.Controller();
	jQuery(function(){
	
		var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", triggerHook: "onLeave", duration: (jQuery(".single_noticia article .miolo").height() - 240 ) })
		.setPin("#pin1")
		.addTo(controller);

	});
};
</script>
<?php
endwhile;
get_footer();
