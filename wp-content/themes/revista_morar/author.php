<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */
?>

<?php
get_header();
$author_id = get_the_author_meta('ID');
?>

<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo', 173) ): ?>
            	<h3><?php the_field('subtitulo', 173); ?></h3>
            <?php endif; ?>
            <?php if( get_field('nome_da_coluna', 'user_'. $author_id ) ): ?>
            	<h1><?php the_field('nome_da_coluna', 'user_'. $author_id ); ?></h1>
            <?php else: ?>
            	<h1><?php the_field('titulo_noticias', 'option'); ?></h1>
            <?php endif; ?>


		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_noticias archive_author">
	<div class="container">
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<?php 


				echo '<ul class="equipe pessoas single"> ';
				     echo '<li>
				     			<div>
				                    <div class="img" style="background-image:url('.get_avatar_url($author_id).')">
				                    </div>
				                    <h4>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</h4>';
				                    if(get_field('cargo', 'user_'. $author_id)){
				                        echo '<span class="cargo">'.get_field('cargo', 'user_'. $author_id ).'</span>';
				                    }
				                    if(get_the_author_meta('description', $author_id)){
				                        echo '<p>'.get_the_author_meta('description', $author_id).'</p>';
				                    }
				    echo '</div></li>';
				echo '</ul>';


				?>
		</div>
		<div class="col-xs-12">









			<?php 

			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
			    $link = "https"; 
			else
			    $link = "http"; 
			$link .= "://"; 
			$link .= $_SERVER['HTTP_HOST']; 
			$link .= $_SERVER['REQUEST_URI']; 

			$pieces = explode("?order=", $link);
			$piecesfil = explode("?filter=", $link);


			$piecesfil2 = explode("?order=", $piecesfil[1]);




			$order = $pieces[1]; 
			$filter = $piecesfil2[0]; 


			$url = $piecesfil[0]; 

			?>
			<div class="filtro">
				<h4>
					Filtros:
				</h4>
				<ul>
					<li class="category">
						<span>Categorias</span>
						<ul>
							<li class="all"><a href="?filter=">Todas</a></li>
						<?php $category_ids = get_all_category_ids(); ?>
						<?php
						$args = array(
						'orderby' => 'name',
						'parent' => 0
						);
						$categories = get_categories( $args );
						foreach ( $categories as $category ) {
						echo '<li class="'.$category->slug.'"><a href="?filter='.$category->slug.'" >'.$category->name.'</a></li>';
						}
					?>
						</ul>
					</li>

				    <li class="order">
				    	<span>Ordenar por:</span>
				    	<ul>
				    		<li class="padrao">
				    			<a href="<?php echo $url; ?>?filter=<?php echo $filter ?>" >
				    				Mais Recentes
				    			</a>
				    		</li>
				    		<li class="date">
				    			<a href="?filter=<?php echo $filter ?>?order=date">
				    				Mais Antigos
				    			</a>
				    		</li>
				    		<li class="views">
				    			<a href="?filter=<?php echo $filter ?>?order=views">
				    				Mais Populares
				    			</a>
				    		</li>
				    		<li class="title">
				    			<a href="?filter=<?php echo $filter ?>?order=title" >
				    				Nome (A - Z)
				    			</a>
				    		</li>
				    	</ul>
				    </li>
				</ul>
			</div>
			<?php 
			$a = 0;

				if ($filter != ''){

					echo '
						<script>
						jQuery(function(){
							jQuery(".filtro li.'.$filter.'").addClass("active");

							jQuery(".filtro .category li.title").addClass("active");
						    var nome = jQuery(".filtro .category li.active a").html();
		        			jQuery(".filtro .category span").html(nome);

							});
						</script>

					';

					if ($order == 'title'){
							echo '
									<script>
									jQuery(function(){
										jQuery(".filtro .order li").removeClass("active");

										jQuery(".filtro .order li.title").addClass("active");
									    var nome = jQuery(".filtro .order li.active a").html();
					        			jQuery(".filtro .order span").html(nome);


										console.log("titulo");

									});

									</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" orderby="title" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" category="'.$filter.'" author="'.$author_id.'"]');

						} elseif ($order == 'date') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.date").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("data");
									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" category="'.$filter.'" author="'.$author_id.'"]');

						} elseif ($order == 'views') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.views").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("views");
									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" orderby="meta_value_num" meta_key="wpb_post_views_count" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" category="'.$filter.'" author="'.$author_id.'"]');

						}  else {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.padrao").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("todos");
									});
								</script>
							';

							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" category="'.$filter.'" author="'.$author_id.'"]');


					}
				} else{
					echo '
						<script>
						jQuery(function(){
							jQuery(".filtro li.all").addClass("active");
							});
						</script>

					';
					if ($order == 'title'){
							echo '
								<script>
									jQuery(function(){
										jQuery(".filtro .order li").removeClass("active");

										jQuery(".filtro .order li.title").addClass("active");
									    var nome = jQuery(".filtro .order li.active a").html();
					        			jQuery(".filtro .order span").html(nome);


										console.log("titulo");

									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" orderby="title" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" author="'.$author_id.'"]');

						} elseif ($order == 'date') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.date").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("data");
									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" order="ASC" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" author="'.$author_id.'"]');

						} elseif ($order == 'views') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.views").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("views");
									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" orderby="meta_value_num" meta_key="wpb_post_views_count" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" author="'.$author_id.'"]');

						}  else {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");
									jQuery(".filtro .order li.padrao").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);
										console.log("todos");
									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="7"  scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" archive="true" no_results_text="<h4>Ainda não temos nenhuma matéria cadastrada.</h4>" author="'.$author_id.'"]');
					}
				}
			?>
				
	    		<?php if(get_field('ad_col_interna', 'option')): ?>
				<div class="separator-wrap ">	
					<?php echo do_shortcode('[the_ad id="'.get_field('ad_col_interna', 'option').'"]'); ?>
				</div>
				<?php endif; ?>
		</div>
	</div>
</section>

<?php
//get_sidebar();
get_footer();
