<?php
/**
 * revista_morar functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package revista_morar
 */

if ( ! function_exists( 'revista_morar_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function revista_morar_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on revista_morar, use a find and replace
		 * to change 'revista_morar' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'revista_morar', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'revista_morar' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'revista_morar_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'revista_morar_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function revista_morar_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'revista_morar_content_width', 640 );
}
add_action( 'after_setup_theme', 'revista_morar_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function revista_morar_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'revista_morar' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'revista_morar' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'revista_morar_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function revista_morar_scripts() {
	wp_enqueue_style( 'revista_morar-style', get_stylesheet_uri() );

	wp_enqueue_script( 'revista_morar-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'revista_morar-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'revista_morar_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}









///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////ADAPTAÇOES JUCAMILLO
// Function to change "posts" to "news" in the admin side menu
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Notícias';
    echo '';
}
add_action( 'admin_menu', 'change_post_menu_label' );







//CLASSE PARENT E CLASSE NOME NO BODY
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );
add_filter('body_class','body_class_section');
function body_class_section($classes) {
    global $wpdb, $post;
    if (is_page()) {
        if ($post->post_parent) {
            $parent  = end(get_post_ancestors($current_page_id));
        } else {
            $parent = $post->ID;
        }
        $post_data = get_post($parent, ARRAY_A);
        $classes[] = 'parent-' . $post_data['post_name'];
    }
    return $classes;
}



//BREADCRUMBS

function custom_breadcrumbs() {
       
    // Settings
    $separator          = '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" class="svg-inline--fa fa-angle-right fa-w-6 fa-3x"><path fill="currentColor" d="M187.8 264.5L41 412.5c-4.7 4.7-12.3 4.7-17 0L4.2 392.7c-4.7-4.7-4.7-12.3 0-17L122.7 256 4.2 136.3c-4.7-4.7-4.7-12.3 0-17L24 99.5c4.7-4.7 12.3-4.7 17 0l146.8 148c4.7 4.7 4.7 12.3 0 17z" class=""></path></svg>';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            } else{

              
                echo '<li><a href="' . get_home_url() . '/blog" title="Blog">Blog</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
                echo '<li><a href="' . get_home_url() . '/blog" title="Blog">Blog</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Resultado de busca por:' . get_search_query() . '">Resultado de busca por: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}
add_shortcode( 'custom_breadcrumbs', 'custom_breadcrumbs' );




//PAGINAÇÃO

function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}



//FUNCAO IF IS POST TYPE
function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) return true;
    return false;
}




//REMOVER 'TITULOS' DOS ARCHIVES

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});


//CRIAR OPTIONS MENU ACF PRO
if( function_exists('acf_add_options_page') ) {
    $user = wp_get_current_user();
    $allowed_roles = array('author');
    if( !array_intersect($allowed_roles, $user->roles ) ) {  
        acf_add_options_page('Informações Gerais');

     } 


}



//PAGINACAO CUSTOM
function custom_pagination($numpages = '', $pagerange = '', $paged='') {if (empty($pagerange)) {$pagerange = 2;}/**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */global $paged;if (empty($paged)) {$paged = 1;}if ($numpages == '') {global $wp_query;$numpages = $wp_query->max_num_pages;if(!$numpages) {$numpages = 1;}}/** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */$pagination_args = array(
            'base'=> get_pagenum_link(1) . '%_%',
            'format'=> 'page/%#%',
            'total'=> $numpages,
            'current'=> $paged,
            'show_all'=> False,
            'end_size'=> 1,
            'mid_size'=> $pagerange,
            'prev_next'=> True,
            'prev_text'=> __('«'),
            'next_text'=> __('»'),
            'type'=> 'plain',
            'add_args'=> false,
            'add_fragment'=> '');

   $paginate_links = paginate_links($pagination_args);
   if ($paginate_links) {echo "<div class='full-pag'><nav class='custom-pagination'>";echo $paginate_links;echo "</nav></div>";}}













//SHORTCODE POSTS RELACIONADOS
function my_related_posts($atts, $content = null) {
     $a = shortcode_atts( array(
        'category' => 'engenharia'
    ), $atts );


    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 4, 
        'orderby'=>'date',
        'order'=>'desc',
        //'post_in'  => get_the_tag_list(),
        'post__not_in' => array($post->ID),
        'tax_query' => array(
            array(
              'taxonomy' => 'category',
              'field' => 'slug',
              'terms' => $a['category']
            )
        )
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="colunas">';
    $a = 0;
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );
        $a ++;


        $string .= '<li class="single up'.$a.'">';

        if ($a == 4) {
            $a = 0;
        } 

        $string .= '
                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" >
                            <div class="conteudo">
                                <div class="topo">';

                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  //make a list item containing a link to the category
                                  $string .= '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                <img src="'.get_field('icone', $cat).'"> 
                                                <span>'.$cat->name.'</span>
                                              </div>';
                                }
                                    
                                $string .= '</div>

                                <div class="txt">
                                    <h3>'.get_the_title().'</h3>
                                    <div class="author">
                                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                                        <div class="info">
                                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                                            <span>'.$cargo.'</span>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="img" style="background-image:url('.$image[0].');">
                            </div>
                        </a>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'my_related_posts', 'my_related_posts' );




//REDIRECIONAMENTO DE SINGLE POSTS LISTAGEM
add_action( 'template_redirect', 'subscription_redirect_post' );

function subscription_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  /*if ( is_single() && 'boleto' ==  $queried_post_type ) {
    wp_redirect( home_url( 'boletos', 'relative' ), 301 );
    exit;
  }*/
  if ( is_single() && 'banner' ==  $queried_post_type ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}




add_filter( 'excerpt_length', function($length) {
    return 20;
} );






//SHORTCODE POSTS RELACIONADOS
function mais_lidos($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 4, 
        'meta_key' => 'wpb_post_views_count', 
        'orderby' => 'meta_value_num', 
        'order' => 'DESC' 
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="noticias">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li>
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <span class="data">
                                    <b>'.get_the_date('d').'</b>
                                    <em>'.get_the_date('M').'</em>
                                </span>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
                                </div>
                            </li>';
    endwhile;
            $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'mais_lidos', 'mais_lidos' );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count;
}

/*
function prefix_limit_post_types_in_search( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array( 'post' ) );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'prefix_limit_post_types_in_search' );
*/





function lwp_2610_custom_author_base() {
    global $wp_rewrite;
    $wp_rewrite->author_base = 'colunista';
}
add_action( 'init', 'lwp_2610_custom_author_base' );










function banner_destaque($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 1, 
        'orderby'=>'rand',
        'order'=>'rand',
        'tax_query' => array(
            array(
              'taxonomy' => 'destaque',
              'field' => 'slug',
              'terms' => 'destaque'
            )
        )
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $string .= '<div class="banner_destaque"  style="background-image:url('.$image[0].');">
                        <div class="container">
                            <div class="col-lg-6 col-md-7 col-sm-9 col-xs-12">';
                                if(get_field('destaque')):
                                $string .= '<a href="'.get_the_permalink().'" title="'.get_the_title().'" class="title">
                                    <span>Destaque</span>
                                    <h3>'.get_field('destaque').'</h3>
                                </a>';
                                endif;

                                $string .= '<h2>'.get_the_title().'</h2>

                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                    SAIBA MAIS
                                </a>
                            </div>
                        </div>
                    </div>';
    endwhile;
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'banner_destaque', 'banner_destaque' );






function novidades_topo($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 3, 
        'orderby'=>'date',
        'order'=>'desc',
        'tax_query' => array(
            array(
              'taxonomy' => 'destaque',
              'field' => 'slug',
              'terms' => 'novidades'
            )
        )
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="novidades_topo">';
    $a = 0;

    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );


        $a ++;

        $string .= '<li class="single up1">';

        if ($a == 3) {
            $a = 0;
        } 

        $string .= '
                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" >
                            <div class="conteudo">
                                <div class="tag">
                                    <img src="'.get_template_directory_uri().'/images/novidades.svg"> 
                                    <span>Novidades</span>
                                </div>

                                <div class="txt">
                                    <h2>'.get_the_title().'</h2>
                                    <p>'.strip_tags( get_the_excerpt() ).'</p>
                                </div>
                            </div>
                            <div class="img" style="background-image:url('.$image[0].');">
                            </div>
                        </a>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'novidades_topo', 'novidades_topo' );





function colunas($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 6, 
        'orderby'=>'date',
        'order'=>'desc',
        'tax_query' => array(
            array(
              'taxonomy' => 'destaque',
              'field' => 'slug',
              'terms' => 'colunas'
            )
        )
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="colunas">';
    $a = 0;

    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );
        $a ++;


        $string .= '<li class="single up'.$a.'">';

        if ($a == 3) {
            $a = 0;
        } 
        $string .= '
                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" >
                            <div class="conteudo">
                                <div class="topo">';

                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  //make a list item containing a link to the category
                                  $string .= '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                <img src="'.get_field('icone', $cat).'"> 
                                                <span>'.$cat->name.'</span>
                                              </div>';
                                }
                                    
                                $string .= '</div>

                                <div class="txt">
                                    <h3>'.get_the_title().'</h3>
                                    <div class="author">
                                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                                        <div class="info">
                                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                                            <span>'.$cargo.'</span>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="img" style="background-image:url('.$image[0].');">
                            </div>
                        </a>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'colunas', 'colunas' );




function colunistas($atts) {

    $users = get_users();
    $string .= '<ul class="colunistas">';
        $i = 0;
    foreach ($users as $user) {
        $i ++ ;
        if ($i < 4){
    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 1, 
        'author'      => $user->ID,
        'orderby'=>'date',
        'order'=>'desc'
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );
        $string .= '<li class="single up1">
                        <div class="conteudo">
                            <div class="txt">
                                <a href="'.get_home_url().'/colunista/'.get_the_author_meta('nickname', $author_id).'" class="author" title="'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'">
                                    <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                                    <div class="info">
                                        <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                                        <span>'.$cargo.'</span>
                                    </div>
                                </a>
                                <h3>
                                    <a href="'.get_the_permalink().'" title="'.get_the_title().'" >
                                    '.get_the_title().'
                                    </a>
                                </h3>
                            </div>
                        </div>
                    </li>';
    endwhile;
    wp_reset_postdata();
    }

    }
    $string .= '</ul>';
    return $string;

}
add_shortcode( 'colunistas', 'colunistas' );




function ultimos_podcasts($atts) {

    $bannerArgs = array( 
        'post_type' => 'podcast', 
        'posts_per_page' => 3, 
        'orderby'=>'date',
        'order'=>'desc'
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="podcast owl-carousel right2">';
    $i = 0;
    $a = 0;

    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );
        $i ++;
        $a ++;

        $string .= '<li class="single ">';

        if ($a == 3) {
            $a = 0;
        } 
 
        $string .= '
                        <a data-fancybox data-src="#hidden-content'.$i.'" href="javascript:;">
                            <div class="conteudo">
                                <div class="topo">';

                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  //make a list item containing a link to the category
                                  $string .= '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                <img src="'.get_field('icone', $cat).'"> 
                                                <span>'.$cat->name.'</span>
                                              </div>';
                                }
                                    
                                $string .= '</div>

                                <div class="txt">';
                                    if(get_field('tempo_de_audio')){
                                        $string .= '<div class="play">
                                                        <img src="'.get_template_directory_uri().'/images/play.svg"> 
                                                        <span>'.get_field('tempo_de_audio').'</span>
                                                    </div>';
                                    }
                                    if(get_field('num_ep')){
                                        $string .= '<span class="num_ep">
                                                    #EP'.get_field('num_ep').'
                                                    </span>';
                                    }

                                    $string .= '<h3>'.get_the_title().'</h3>
                                    <div class="author">
                                        <div class="foto" style="background-image:url('.get_avatar_url($author_id).')"></div>
                                        <div class="info">
                                            <strong>'.get_the_author_meta('first_name', $author_id).' '.get_the_author_meta('last_name', $author_id).'</strong>
                                            <span>'.$cargo.'</span>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="img" style="background-image:url('.$image[0].');">
                            </div>
                        </a>

                        <div class="iframe" style="display: none;" id="hidden-content'.$i.'">
                            '.get_field('iframe_soundcloud').'
                        </div>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();











    return $string;
}
add_shortcode( 'ultimos_podcasts', 'ultimos_podcasts' );






function ultimo_video($atts) {

    $bannerArgs = array( 
        'post_type' => 'video', 
        'posts_per_page' => 1, 
        'offset'=>1,
        'orderby'=>'date',
        'order'=>'desc'
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="video only">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );
        $string .= '<li class="single up1">
                        <a data-fancybox href="'.get_field('video', false, false).'" class="video-responsive" >
                            <div class="conteudo">
                                <div class="topo">
                                <img src="'.get_template_directory_uri().'/images/play-video.svg">';                                    
                                $string .= '</div>

                                <div class="txt">';
                                    $categories = wp_get_post_categories( get_the_ID() );
                                    foreach($categories as $c){
                                      $cat = get_category( $c );
                                      //get the name of the category
                                      $cat_id = get_cat_ID( $cat->name );
                                      //make a list item containing a link to the category
                                      $string .= '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                    <img src="'.get_field('icone', $cat).'"> 
                                                    <span>'.$cat->name.'</span>
                                                  </div>';
                                    }

                                    $string .= '<h3>'.get_the_title().'</h3>
                                </div>';

                                if(get_field('tempo_de_video')){
                                    $string .= '<span class="time">'.get_field('tempo_de_video').'</span>';
                                }

                            $string .= '</div>
                            <div class="img" style="background-image:url('.$image[0].');">
                            </div>
                        </a>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'ultimo_video', 'ultimo_video' );



function lista_ultimos_video($atts) {

    $bannerArgs = array( 
        'post_type' => 'video', 
        'posts_per_page' => 3, 
        'orderby'=>'date',
        'order'=>'desc',
        'offset'=>1,
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="video list">';

    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $author_id = get_the_author_meta('ID');
        $cargo = get_field('cargo', 'user_'. $author_id );

        $string .= '<li class="single up1">
                        <a data-fancybox href="'.get_field('video', false, false).'">
                            <div class="side">
                            <div class="video-responsive">
                                <div class="conteudo">
                                    <div class="topo">
                                        <img src="'.get_template_directory_uri().'/images/play-video.svg">'; 
                                    $string .= '</div>';

                                    if(get_field('tempo_de_video')){
                                        $string .= '<span class="time">'.get_field('tempo_de_video').'</span>';
                                    }

                                $string .= '</div>
                                <div class="img" style="background-image:url('.$image[0].');">
                                </div>
                            </div>
                            </div>
                            <div class="info">
                                <div class="txt">';
                                    $categories = wp_get_post_categories( get_the_ID() );
                                    foreach($categories as $c){
                                      $cat = get_category( $c );
                                      //get the name of the category
                                      $cat_id = get_cat_ID( $cat->name );
                                      //make a list item containing a link to the category
                                      $string .= '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                    <img src="'.get_field('icone', $cat).'"> 
                                                    <span>'.$cat->name.'</span>
                                                  </div>';
                                    }

                                    $string .= '<h3>'.get_the_title().'</h3>
                                </div>
                            </div>
                        </a>
                    </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'lista_ultimos_video', 'lista_ultimos_video' );














function redes_sociais($atts) {

    if( have_rows('redes_sociais', 'option') ):
        $string .= '<ul class="social"> ';
        while( have_rows('redes_sociais', 'option') ): the_row();
             $string .= '<li>
                <a href="'.get_sub_field('link').'" target="_blank">
                    '.get_sub_field('icone').'
                </a>
            </li>';
        endwhile;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'redes_sociais', 'redes_sociais' );










function equipe($atts) {

    if( have_rows('equipe') ):
        $string .= '<ul class="equipe pessoas"> ';
        while( have_rows('equipe') ): the_row();
             $string .= '<li>
                            <div>
                            <div class="img" style="background-image:url('.get_sub_field('foto').')">
                            </div>
                            <h4>'.get_sub_field('nome').'</h4>';

                            if(get_sub_field('cargo')){
                                $string .= '<span class="cargo">'.get_sub_field('cargo').'</span>';
                            }
                            if(get_sub_field('descricao')){
                                $string .= '<p>'.get_sub_field('descricao').'</p>';
                            }

            $string .= '</div></li>';
        endwhile;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'equipe', 'equipe' );





function todos_colunistas($atts) {

    $a = 0;
    $blogusers = get_users( [ 'role__in' => [ 'author', 'contributor', 'administrator', 'editor' ] ] );
    // Array of WP_User objects.
        $string .= '<ul class="equipe pessoas"> ';
        foreach ( $blogusers as $user ) {
            $a ++;


            $string .= '<li class="up'.$a.'">';

            if ($a == 4) {
                $a = 0;
            } 
             $string .= '
                            <a href="'.get_home_url().'/colunista/'.get_the_author_meta('nickname', $user->ID).'" title="'.get_the_author_meta('first_name', $user->ID).' '.get_the_author_meta('last_name', $user->ID).'">
                            <div class="img" style="background-image:url('.get_avatar_url($user->ID).')">
                            </div>
                            <h4>'.get_the_author_meta('first_name', $user->ID).' '.get_the_author_meta('last_name', $user->ID).'</h4>';
                            if(get_field('cargo', 'user_'. $user->ID)){
                                $string .= '<span class="cargo">'.get_field('cargo', 'user_'. $user->ID ).'</span>';
                            }
                            if(get_the_author_meta('description', $user->ID)){
                                $string .= '<p>'.get_the_author_meta('description', $user->ID).'</p>';
                            }
                $string .= '<span class="btn_light">LER MATÉRIAS</span>';
            $string .= '</a></li>';
        }
        $string .= '</ul>';

    return $string;
}
add_shortcode( 'todos_colunistas', 'todos_colunistas' );





// these call the appropriate function based on the action passed from the data object in the js
add_action( 'wp_ajax_process_shortcode_on_image_click_action', 'process_shortcode_on_image_click_ajax');
add_action( 'wp_ajax_nopriv_process_shortcode_on_image_click_action', 'process_shortcode_on_image_click_ajax');

function process_shortcode_on_image_click_ajax() {
    // you should check for a nonce and do other validation here to make sure this is a legit request
    echo do_shortcode('[todos_colunistas]');
    die();
}