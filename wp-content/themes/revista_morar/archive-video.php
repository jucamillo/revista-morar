<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */

get_header();
?>
<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_video', 'option') ): ?>
            	<h3><?php the_field('subtitulo_video', 'option'); ?></h3>
            <?php endif; ?>
            <?php if( get_field('titulo_video', 'option') ): ?>
            	<h1><?php the_field('titulo_video', 'option'); ?></h1>
            <?php endif; ?>
		</div>
	</div>
</section>
<section class="topo destaque_archive archive_video">
	<div class="container">
		<div class="col-xs-12">
			<h2>DESTAQUE</h2>
			<?php
			    $bannerArgs = array( 
			        'post_type' => 'video', 
			        'posts_per_page' => 1, 
			        'orderby'=>'id',
			        'order'=>'desc'
			    );
			    $bannerLoop = new WP_Query( $bannerArgs ); 
			    echo '<ul class="video only mobile">';
			    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
			        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			        $author_id = get_the_author_meta('ID');
			        $cargo = get_field('cargo', 'user_'. $author_id );
			        echo '<li class="single ">
			                        <a data-fancybox href="'.get_field('video', false, false).'" class="video-responsive" >
			                            <div class="conteudo">
			                                <div class="topo">
			                                <img src="'.get_template_directory_uri().'/images/play-video.svg">';                                    
			                                echo '</div>

			                                <div class="txt">';
			                                    $categories = wp_get_post_categories( get_the_ID() );
			                                    foreach($categories as $c){
			                                      $cat = get_category( $c );
			                                      //get the name of the category
			                                      $cat_id = get_cat_ID( $cat->name );
			                                      //make a list item containing a link to the category
			                                      echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
			                                                    <img src="'.get_field('icone', $cat).'"> 
			                                                    <span>'.$cat->name.'</span>
			                                                  </div>';
			                                    }

			                                    echo '<h3>'.get_the_title().'</h3>
			                                </div>';

			                                if(get_field('tempo_de_video')){
			                                    echo '<span class="time">'.get_field('tempo_de_video').'</span>';
			                                }

			                            echo '</div>
			                            <div class="img" style="background-image:url('.$image[0].');">
			                            </div>
			                        </a>
			              </li>';
			    endwhile;
			    echo '</ul>';




			    echo '<ul class="video list desktop">';
			    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
			        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			        $author_id = get_the_author_meta('ID');
			        $cargo = get_field('cargo', 'user_'. $author_id );
			        
        			echo '<li class="single">
                        <a data-fancybox href="'.get_field('video', false, false).'">
                            <div class="side">
                            <div class="video-responsive">
                                <div class="conteudo">
                                    <div class="topo">
                                        <img src="'.get_template_directory_uri().'/images/play-video.svg">'; 
                                    echo '</div>';

                                    if(get_field('tempo_de_video')){
                                        echo '<span class="time">'.get_field('tempo_de_video').'</span>';
                                    }

                                echo '</div>
                                <div class="img" style="background-image:url('.$image[0].');">
                                </div>
                            </div>
                            </div>
                            <div class="info">
                                <div class="txt">';
                                    $categories = wp_get_post_categories( get_the_ID() );
                                    foreach($categories as $c){
                                      $cat = get_category( $c );
                                      //get the name of the category
                                      $cat_id = get_cat_ID( $cat->name );
                                      //make a list item containing a link to the category
                                      echo '<div class="tag" style="background:'.get_field('cor', $cat).';">
                                                    <img src="'.get_field('icone', $cat).'"> 
                                                    <span>'.$cat->name.'</span>
                                                  </div>';
                                    }

                                    echo '<h3>'.get_the_title().'</h3>
                                    <p>'.get_the_excerpt().'</p>
                                </div>
                            </div>
                        </a>
                    </li>';
			    endwhile;
			    echo '</ul>'; 






			    ?>

		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_video">
	<div class="container">
		<div class="col-xs-12 up1">
    		<?php if(get_field('ad_video', 'option')): ?>
			<div class="separator-wrap top">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_video', 'option').'"]'); ?>
			</div>
			<?php endif; ?>
			<h2>VÍDEOS ANTERIORES</h2>
			<?php echo do_shortcode('[ajax_load_more container_type="ul" css_classes="video list" post_type="video" posts_per_page="6" offset="1" scroll="false" button_label="CARREGAR MAIS VÍDEOS" button_loading_label="CARREGANDO VÍDEOS..." button_done_label="TODOS OS VÍDEOS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum vídeo cadastrado."]');?>


		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
