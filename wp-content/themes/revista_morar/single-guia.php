<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package revista_morar
 */

get_header();
?>


		<?php
		while ( have_posts() ) :
			the_post(); 
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

?>

<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo', 250) ): ?>
            	<h3><?php the_field('subtitulo', 250); ?></h3>
            <?php endif; ?>
            <h1><?php echo get_the_title( 250 ); ?> </h1>
		</div>
	</div>
</section>
<section class="miolo list-archive-section archive_guia ">
	<div class="container">
		<div class="col-xs-12">
			<a href="<?php echo get_home_url(); ?>/guia" title="Voltar" class="back-btn">
				<i class="fas fa-chevron-left"></i> <span>Voltar</span>
			</a>
			<div class="single_guia">
				<div class="col-lg-3 col-md-4 col-xs-12">
					<ul class="pessoas">
						<?php
							echo '<li>
                            <div>
                            <div class="img" style="background-image:url('.$image[0].')">
                            </div>
                            <h4>'.get_the_title().'</h4>';

                            if(get_field('cargo')){
                                echo '<span class="cargo">'.get_field('cargo').'</span>';
                            }
	                	echo '<p>'.get_the_excerpt().'</p>';

            			echo '</div></li>'; ?>
					</ul>
				</div>
				<div class="col-lg-9 col-md-8 col-xs-12">
					<div class="info">
							
						<h3>
							Serviço:
							<?php
							$terms = get_the_terms( $post->ID , 'servico' );
				            foreach ( $terms as $term ) {
				                echo  '<span>'.$term->name.'</span>';
				            }
				            ?>
						</h3>
						<?php the_content(); ?>		

						<h3>
							Contatos
						</h3>
						<ul class="contato">
							<li>
								Telefone

				                <?php if( get_field('telefone_1') ): ?>
				                <a href="tel:<?php the_field('telefone_1'); ?>" target="_blank"  title="Ligar"><?php the_field('telefone_1'); ?></a>
				                <?php endif; ?>


				                <?php if( get_field('telefone_2') ): ?>
				                <a href="tel:<?php the_field('telefone_2'); ?>" target="_blank"  title="Ligar"><?php the_field('telefone_2'); ?></a>
				                <?php endif; ?>
							</li>
				            <?php if( get_field('email') ): ?>
							<li>
								Email

				                <a href="mailto:<?php the_field('email'); ?>" target="_blank" title="Enviar email"><?php the_field('email'); ?></a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('endereco') ): ?>
							<li>
								Endereço
				                <address><?php the_field('endereco'); ?></address>
							</li>
				            <?php endif; ?>
						</ul>



						<h3>
							Redes Sociais
						</h3>
						<ul class="social">
				            <?php if( get_field('instagram') ): ?>
							<li>
								<a href="<?php the_field('instagram'); ?>" target="_blank">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('facebook') ): ?>
							<li>
								<a href="<?php the_field('facebook'); ?>" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('linkedin') ): ?>
							<li>
								<a href="<?php the_field('linkedin'); ?>" target="_blank">
									<i class="fab fa-linkedin-in"></i>
								</a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('youtube') ): ?>
							<li>
								<a href="<?php the_field('youtube'); ?>" target="_blank">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('twitter') ): ?>
							<li>
								<a href="<?php the_field('twitter'); ?>" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
				            <?php endif; ?>
				            <?php if( get_field('pinterest') ): ?>
							<li>
								<a href="<?php the_field('pinterest'); ?>" target="_blank">
									<i class="fab fa-pinterest-p"></i>
								</a>
							</li>
				            <?php endif; ?>
							
						</ul>
					</div>
				</div>
			</div>

    		<?php if(get_field('ad_guia_interna', 'option')): ?>
			<div class="separator-wrap">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_guia_interna', 'option').'"]'); ?>
			</div>
			<?php endif; ?>

		</div>
	</div>

</section>
<?php
endwhile;
get_footer();
