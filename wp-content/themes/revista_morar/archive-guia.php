<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */

get_header();
?>
<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo', 250) ): ?>
            	<h3><?php the_field('subtitulo', 250); ?></h3>
            <?php endif; ?>
            <h1><?php echo get_the_title( 250 ); ?> </h1>
		</div>
	</div>
</section>
<section class="miolo list-archive-section archive_guia">
	<div class="container">
		<div class="col-xs-12">





			<?php 

			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
			    $link = "https"; 
			else
			    $link = "http"; 
			$link .= "://"; 
			$link .= $_SERVER['HTTP_HOST']; 
			$link .= $_SERVER['REQUEST_URI']; 

			$pieces = explode("?order=", $link);
			$piecesfil = explode("?filter=", $link);
			$piecesserv = explode("?service=", $link);


			$piecesfil2 = explode("?", $piecesfil[1]);


			$piecesserv2 = explode("?", $piecesserv[1]);




			$order = $pieces[1]; 
			$filter = $piecesfil2[0]; 
			$service = $piecesserv2[0]; 


			$url = $piecesfil[0]; 

			?>
			<div class="filtro">
				<h4>
					Filtros:
				</h4>
				<ul>
					<li class="categories">
						<span>Categorias</span>
						<ul >
							<li class="all active"><a href="?filter=">Todas</a></li>
						<?php $category_ids = get_all_category_ids(); ?>
						<?php
						$args = array(
						'orderby' => 'name',
						'parent' => 0
						);
						$categories = get_categories( $args );
						foreach ( $categories as $category ) {
						echo '<li class="'.$category->slug.'"><a href="?filter='.$category->slug.'" >'.$category->name.'</a></li>';
						}
					?>
						</ul>
					</li>

					<li class="services">
						<span>Todos os serviços</span>
						<ul>
							<li class="all"><a href="?service=">Todos os serviços</a></li>
						<?php 
						$servicos = get_terms([
						    'taxonomy' => 'servico',
						    'hide_empty' => false,
							'orderby' => 'name',
							'parent' => 0
						]);

						foreach ( $servicos as $serv ) {
						echo '<li class="'.$serv->slug.'"><a href="?service='.$serv->slug.'" >'.$serv->name.'</a></li>';
						}
					?>
						</ul>
					</li>
					<?php

				
					if ($service != ''){ ?>

				    <li class="order">
				    	<span>Ordenar por</span>
				    	<ul class="">
				    		<li class="padrao active">
				    			<a href="<?php echo $url; ?>?service=<?php echo $service ?>" >
				    				Mais Recentes
				    			</a>
				    		</li>
				    		<li class="date">
				    			<a href="?service=<?php echo $service ?>?order=date">
				    				Mais Antigos
				    			</a>
				    		</li>
				    		<li class="views">
				    			<a href="?service=<?php echo $service ?>?order=views">
				    				Mais Populares
				    			</a>
				    		</li>
				    		<li class="title">
				    			<a href="?service=<?php echo $service ?>?order=title" >
				    				Nome (A - Z)
				    			</a>
				    		</li>
				    	</ul>
				    </li>

					<?php }else{ ?>

				    <li>
				    	<span>Ordenar por</span>
				    	<ul class="order">
				    		<li class="padrao">
				    			<a href="<?php echo $url; ?>?filter=<?php echo $filter ?>" >
				    				Mais Recentes
				    			</a>
				    		</li>
				    		<li class="date">
				    			<a href="?filter=<?php echo $filter ?>?order=date">
				    				Mais Antigos
				    			</a>
				    		</li>
				    		<li class="views">
				    			<a href="?filter=<?php echo $filter ?>?order=views">
				    				Mais Populares
				    			</a>
				    		</li>
				    		<li class="title">
				    			<a href="?filter=<?php echo $filter ?>?order=title" >
				    				Nome (A - Z)
				    			</a>
				    		</li>
				    	</ul>
				    </li>

					<?php } ?>
				</ul>
			</div>
			<?php 

				if ($filter != ''){

					echo '
						<script>
						jQuery(function(){
							jQuery(".filtro .order li.'.$filter.'").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .services order").html(nome);
							});
						</script>

					';

					if ($order == 'title'){
							echo '
								<script>

								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.title").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado." 

								order="ASC" orderby="title" category="'.$filter.'" ]');

						} elseif ($order == 'date') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.date").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

								order="ASC" category="'.$filter.'"]');

						} elseif ($order == 'views') {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.views").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
								</script>

							';
							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

								orderby="meta_value_num" meta_key="wpb_post_views_count" category="'.$filter.'"]');

						}  else {
							echo '
								<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.padrao").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
								</script>

							';

							echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

								category="'.$filter.'"]');


					}
				} else{
					echo '
						<script>
						jQuery(function(){
							jQuery(".filtro .services li.all").addClass("active");
								    var nome = jQuery(".filtro .services li.active a").html();
				        			jQuery(".filtro .services span").html(nome);
							});
						</script>

					';

					if ($service != ''){

						echo '
							<script>
							jQuery(function(){
								jQuery(".filtro .services li.'.$service.'").addClass("active");
								    var nome = jQuery(".filtro .services li.active a").html();
				        			jQuery(".filtro .services span").html(nome);
								});



							</script>

						';

						if ($order == 'title'){
								echo '
									<script>
										jQuery(function(){
											jQuery(".filtro .order li").removeClass("active");

											jQuery(".filtro .order li.title").addClass("active");
										    var nome = jQuery(".filtro .order li.active a").html();
						        			jQuery(".filtro .order span").html(nome);

											});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado." 
									taxonomy="servico" taxonomy_terms="'.$service.'" taxonomy_operator="IN"
									order="ASC" orderby="title" ]');

							} elseif ($order == 'date') {
								echo '
									<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.date").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."
									taxonomy="servico" taxonomy_terms="'.$service.'" taxonomy_operator="IN"
									order="ASC"]');

							} elseif ($order == 'views') {
								echo '
									<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.views").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."
									taxonomy="servico" taxonomy_terms="'.$service.'" taxonomy_operator="IN"

									orderby="meta_value_num" meta_key="wpb_post_views_count"]');

							}  else {
								echo '
									<script>
									jQuery(function(){
										jQuery(".filtro li.padrao").addClass("active");
										});
									</script>

								';

								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."
									taxonomy="servico" taxonomy_terms="'.$service.'" taxonomy_operator="IN"]');


						}
					} else{
						if ($order == 'title'){
								echo '
									<script>
										jQuery(function(){
											jQuery(".filtro .order li").removeClass("active");

											jQuery(".filtro .order li.title").addClass("active");
										    var nome = jQuery(".filtro .order li.active a").html();
						        			jQuery(".filtro .order span").html(nome);

											});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

									order="ASC" orderby="title" ]');

							} elseif ($order == 'date') {
								echo '
									<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.date").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

								 	order="ASC"]');

							} elseif ($order == 'views') {
								echo '
									<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.views").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8"  scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."

									 orderby="meta_value_num" meta_key="wpb_post_views_count"]');

							}  else {
								echo '
									<script>
								jQuery(function(){
									jQuery(".filtro .order li").removeClass("active");

									jQuery(".filtro .order li.padrao").addClass("active");
								    var nome = jQuery(".filtro .order li.active a").html();
				        			jQuery(".filtro .order span").html(nome);

									});
									</script>

								';
								echo do_shortcode('[ajax_load_more container_type="ul" css_classes="pessoas list" post_type="guia" posts_per_page="8" scroll="false" button_label="CARREGAR MAIS PROFISSIONAIS" button_loading_label="CARREGANDO PROFISSIONAIS..." button_done_label="TODOS OS PROFISSIONAIS FORAM CARREGADOS" archive="true" no_results_text="Ainda não temos nenhum profissional cadastrado."]');
						}
					}

				}





			?>





    		<?php if(get_field('ad_guia', 'option')): ?>
			<div class="separator-wrap">	
				<?php echo do_shortcode('[the_ad id="'.get_field('ad_guia', 'option').'"]'); ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
