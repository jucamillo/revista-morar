<?php
/* Template Name: Arquivo Notícias */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package revista_morar
 */

get_header();
?>

		<?php
		while ( have_posts() ) :
			the_post(); ?>

<section class="title_section" style="background-image: url(<?php the_field('bg_titulo', 'option'); ?>);">
	<div class="container">
		<div class="col-xs-12">
            <?php if( get_field('subtitulo_noticias', 'option') ): ?>
            	<h3><?php the_field('subtitulo_noticias', 'option'); ?></h3>
            <?php endif; ?>
            <?php if( get_field('titulo_noticias', 'option') ): ?>
            	<h1><?php the_field('titulo_noticias', 'option'); ?></h1>
            <?php endif; ?>
		</div>
	</div>
</section>

<section class="miolo list-archive-section archive_noticias">
	<div class="container">
		<div class="col-xs-12">
			<div class="filtro">
				<h4>
					Filtros:
				</h4>
				<ul>
				    <?php wp_list_categories( array(
				        'orderby'    => 'name',
				        'show_count' => false,
        				'title_li'   => '<span>Categorias</span>',
				    ) ); ?> 

				    <li>
				    	<span>Ordenar por:</span>
				    	<ul class="order">
				    		<li>
				    			<a href="<?php echo get_home_url(); ?>/noticias">
				    				Mais recentes
				    			</a>
				    		</li>
				    		<li>
				    			<a href="<?php echo get_home_url(); ?>/noticias/date_asc">
				    				Mais antigos
				    			</a>
				    		</li>
				    		<li>
				    			<a href="<?php echo get_home_url(); ?>/noticias/popular">
				    				Mais Populares
				    			</a>
				    		</li>
				    		<li>
				    			<a href="<?php echo get_home_url(); ?>/noticias/nome">
				    				Nome (A - Z)
				    			</a>
				    		</li>
				    	</ul>
				    </li>
				</ul>
			</div>

			<?php echo do_shortcode('[ajax_load_more container_type="ul" css_classes="colunas" post_type="post" posts_per_page="8" offset="1" scroll="false" button_label="CARREGAR MAIS MATÉRIAS" button_loading_label="CARREGANDO MATÉRIAS..." button_done_label="TODOS AS MATÉRIAS FORAM CARREGADAS" archive="true" no_results_text="Ainda não temos nenhuma matéria cadastrada."]');?>

		</div>
	</div>
</section>


		<?php endwhile;  ?>

<?php
get_footer();
